%%%  Interpreter
%%%
%%%  Last update: Wed Apr 7 2:30pm

% Concrete Syntax in the style of C++                     Abstract Syntax Records
%================================================================================
% <s> ::= ';'                                             % skipStmt
%       | '{' <s1> { ';' <s2> } '}'                       % seqStmt(S1 S2)
%                                                              --See note below
%       | new <x> <s>                                     % newvarStmt(X S)
%                                                              --ie. 'local'
%       | <x1> = <x2>                                     % vareqStmt(X1 X2)
%       | <x> = <v>                                       % valeqStmt(X V)
%       | if '(' <x> ')' <s1> else <s2>                   % ifStmt(X S1 S2)
%       | <x>'(' <y1> { ',' <yn> } ')'                          % fappStmt(X Ys)
%                                                         % fprim(X Ys)
%
% NOTE: fappStmt and fprim have the same concrete syntax.  The difference is
% that fappStmt is for user-defined functions and fprim is for built-in 
% functions. For your project, this mean that if X is an identifier, then
% it's an fappStmt, but if it's one of 'cout' '+' '-' '*' '/' '==' etc.,
% then it's an fprim.
% ALSO, there can be more than two statements in a sequence. { <s1>;<s2>;<s3> }
% would be seqStmt(S1 seqStmt(S2 S3)). 
%
%-----------------------------------------------------------------------------
% <v> ::= <int>                                           % <int>
%       | <bool>                                          % true | false
%       | 'anon(' <x1>...<xn> ')' '{' <s> '}'             % fdef(Xs S)
%

declare
[U]={Module.link ['executionutils.ozf']}  % The module that contains the utils below.
                                          % Needs to be located in the same directory as your
                                          % other Oz files, and in the directory from which
                                          % Oz is run.  Alternatively, include a
                                          % the file name.

declare                                        % -----Stack Operations----------
MakeEmptyStack=U.makeEmptyStack         % proc {MakeEmptyStack ?OutStack}
StackIsEmpty=U.stackIsEmpty             % proc {StackIsEmpty InStack ?Bool}
PopSemStack=U.popSemStack               % proc {PopSemStack InStack ?Stmt#E|OutStack}
PushSemStack=U.pushSemStack             % proc {PushSemStack Stmt#E InStack ?OutStack}

declare                                        % -----Store Operations----------
NewLocnInStore=U.newLocnInStore         % proc {NewLocnInStore ?Locn}
LookupInStore=U.lookupInStore           % proc {LookupInStore Locn ?Value}
BindLocnValInStore=U.bindLocnValInStore % proc {BindLocnValInStore Locn Val}
StoreContents=U.storeContents           % proc {StoreContents ?ListOfLocnValPairs}

declare                                        % -----Environment Operations----------
NewEnv=U.newEnv                         % proc {NewEnv ?Env}
LookupInEnv=U.lookupInEnv               % proc {LookupInEnv Identifier Env ?StoreLocn}
RestrictE=U.restrictE                   % proc {RestrictE Idents Env ?NewEnv}
AddMappingE=U.addMappingE               % proc {AddMappingE IdentLocnPairs Env ?NewEnv}
EnvContents=U.envContents               % proc {EnvContents Env ?ListofIdentLocnPairs}

% Three procedures to be written:
ExecuteProgram
ExecuteStatement
CreateVal
proc {ExecuteProgram Program}	
	local
		E = {NewEnv}
	
		L1 = {NewLocnInStore}
		{BindLocnValInStore L1 Show}

		L2 = {NewLocnInStore}
		{BindLocnValInStore L2 Number.'+'}

		L3 = {NewLocnInStore}
		{BindLocnValInStore L3 Number.'-'}

		L4 = {NewLocnInStore}
		{BindLocnValInStore L4 Number.'*'}
		
		L5 = {NewLocnInStore}
		{BindLocnValInStore L5 Value.'=='}
		
		L6 = {NewLocnInStore}
		{BindLocnValInStore L6 Int.'div'}
		
		Env = {AddMappingE [cout#L1 '+'#L2 '-'#L3 '*'#L4 '=='#L5 '/'#L6] E }
		
		S = {MakeEmptyStack}
		SF = {PushSemStack Program#Env S}
	in
		{Show "------------------------------------------------------"}
		{Show "Ejecutando"}
		{ExecuteStatement SF}
		{Show "Finalizado"}
	end
end

proc {ExecuteStatement Stack}
	
	if {Not {StackIsEmpty Stack}} then 
		local
			LocalStore 
			NewEnv
			NuevoStack
			AuxStack
			
			Stmt#E|OutStack = {PopSemStack Stack}
			case Stmt
			of skipStmt then
			   {Show " > skipStmt"}
				NuevoStack = OutStack
				
			[] fprim(X Ys) then
				Args = {Map Ys fun {$ Y} {LookupInStore {LookupInEnv Y E}} end} in 
				{Show " > fprim"}
				{Procedure.apply {LookupInStore {LookupInEnv X E}} Args}
				NuevoStack = OutStack
				
			[] seqStmt(S1 S2) then 
				{Show " > seqStmt"}
				AuxStack = {PushSemStack S2#E OutStack}
				NuevoStack = {PushSemStack S1#E AuxStack}
				
			[] newvarStmt(X S) then Variable in 
				{Show " > newvarStmt"}
				LocalStore = {NewLocnInStore}%nuevo variable
				{BindLocnValInStore LocalStore Variable}  %asinga una ubicación al valor
				NewEnv = {AddMappingE [X#LocalStore] E}   %crea env apartir de otro
			   NuevoStack = {PushSemStack S#NewEnv OutStack} 
			   
			[] vareqStmt(X1 X2) then LookS1 LookS2 in %%variable -variable
			   {Show " > vareqStmt"}
			   LookS1 = {LookupInStore X1 E}
			   LookS2 = {LookupInStore X2 E}
			   {BindLocnValInStore LookS1 LookS2}
			   {BindLocnValInStore LookS2 LookS1}
			   NuevoStack = OutStack
			   
			[] valeqStmt(X Variable) then Look in%variable valor
			   {Show " > vareqStmt"}
			   Look = {LookupInStore X E} 
			   {BindLocnValInStore Look {CreateVal Variable E}}
			   NuevoStack = OutStack
				
			[] ifStmt(X Stmt1 Stmt2) then 
			    {Show " > ifStmt"}
			   if {LookupInStore {LookupInEnv X E}} then
			      {Show " > Stmt1"}
			      NuevoStack = {PushSemStack Stmt1#E OutStack}
			   else {Show " > Stmt2"}
				 NuevoStack = {PushSemStack Stmt2#E OutStack}
			   end

			in
			{ExecuteStatement NuevoStack} 
		end
	end
end


fun {CreateVal Variable E}
% The code below creates the procedure {Free Stmt} that returns a list of the identifiers
% that are free in Stmt.  It will be needed by CreateVal above in creating a procedure value.
% Be sure to take the time to look through this code and understand it.  It can be helpful as
% a model for ExecuteStatement, and you will be expected to understand it for the midterm exam.
local
   Remove = Record.subtract
   Add = fun{$ Fr X}
	    {AdjoinAt Fr X unit}
	 end
   AddList = fun{$ Fr Xs}
		{AdjoinList Fr {Map Xs fun{$ X} X#unit end}}
	     end
   SubtractList = fun{$ Fr Xs}
		     {FoldL Xs fun {$ X Y} {Record.subtract X Y} end Fr}
		end   

   fun {FreeVars St Dest} % Dest is a record of all free vars at "this" scope
      case St of skipStmt then Dest
      [] seqStmt(S1 S2) then
	 T={FreeVars S1 Dest} in 
	 {FreeVars S2 T}
      [] newvarStmt(X S) then
	 T={FreeVars S Dest} in
	 {Remove T X}
      [] vareqStmt(X1 X2) then
	 T={Add Dest X1} in
	 {Add T X2}
      [] valeqStmt(X V) then
	 T in 
	 case V of fdef(Xs S) then
	    T={U.subtractList {AddList {FreeVars S Dest} Xs} Xs}
	 else T=Dest end
	 {Add T X}
      [] ifStmt(X S1 S2) then
	 T1={Add Dest X}
	 T2={FreeVars S1 T1} in
	 {FreeVars S2 T2}
      [] fappStmt(X Ys) then
	 T={Add Dest X} in
	 {AddList T Ys}
      [] fprim(ExternalProcedure Args) then
	 {AddList Dest ExternalProcedure|Args}
      end
   end
in
   fun {Free St}
      Dest=freevars() in
      {Record.arity {FreeVars St Dest}}
   end
end

%% Abstract Syntax of Example programs.
%% ---YOU WILL NEED TO WRITE SOME ADDITIONAL ONES TO COMPLETELY TEST YOUR CODE
Program1=skipStmt

Programa_test = fprim('cout' ['uno'])

%%%%% Test SkipStmt

{ExecuteProgram Program1}
{ExecuteProgram Programa_test}

%%%%%%%%%%%%%%%%%%%



Program2=newvarStmt('x' seqStmt(valeqStmt('x' 3) fprim(cout ['x'])))
%The following programs are from HW 4 Q# 5
P1=newvarStmt('x'
	      seqStmt(seqStmt(valeqStmt('x' 1)
			      newvarStmt('x'
					 seqStmt(valeqStmt('x' 2)
						 fprim('cout' ['x']))))
		      fprim('cout' ['x'])))
P2a=newvarStmt('Res'
	       seqStmt(newvarStmt('Arg1'
				  newvarStmt('Arg2'
					     seqStmt(seqStmt(valeqStmt('Arg1' 7)
							     valeqStmt('Arg2' 6))
						     fprim('*' ['Arg1' 'Arg2' 'Res']))))
		       fprim('cout' ['Res'])))
P3=newvarStmt('X'
	      newvarStmt('xtimesy'
			 newvarStmt('tmp1'
				    newvarStmt('tmp2'
					       seqStmt(seqStmt(valeqStmt('X' 2)
							       valeqStmt('xtimesy'
									 fdef(['Y' 'Res']
									      fprim('*' ['X' 'Y' 'Res']))))
						       seqStmt(seqStmt(valeqStmt('tmp1' 3)
								       fappStmt('xtimesy' ['tmp1' 'tmp2']))
							       fprim('cout' ['tmp2'])))))))
						       
in {ExecuteProgram Program2} % Call to execute a program
%probanco programa

